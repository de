#include <math.h>
#include <stdio.h>
#include <gsl/gsl_sf_lambert.h>

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  for (int a = 2; a < 8; ++a)
  {
    double log_a = log(a);
    for (int p = 2; p < 16; ++p)
    {
      double log_p = log(p);
      for (int r = 1; r < 16; ++r)
      {
        char filename[100];
        snprintf(filename, 100, "a%02d-p%02d-r%02d.dat", a, p, r);
        FILE *f = fopen(filename, "wb");
        double log_R = log(2) * r;
        int samples = 1000;
        for (int s = 0; s < samples; ++s)
        {
          double t = s / (double) samples;
          double log_z = t * log_a + pow(p, t) * log_R;
          double x = pow(p, log_z / log_a) * log_p * log_R / log_a;
          gsl_sf_result result;
          gsl_sf_lambert_W0_e(x, &result);
          double W_lo = result.val - result.err;
          double W_hi = result.val + result.err;
          double W_approx = log(x) - log(log(x));
          fprintf(f, "%.18g\t%.18g\t%.18g\t%.18g\n", x, W_lo, W_hi, W_approx);
        }
        fclose(f);
      }
    }
  }
  return 0;
}
