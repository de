#include <cassert>
#include <tuple>

// extended exponent range

template <typename M, typename E>
struct floatx
{
  M m;
  E e;

  floatx(const M &m0, const E &e0)
  {
    int e1;
    m = frexp(m0, &e1);
    if (m == 0 || isinf(m) || isnan(m))
    {
      e = 0;
    }
    else
    {
      e = e0 + e1;
    }
  }

  floatx(const M &m0) : floatx(m0, E(0)) { }
  floatx() : floatx(M(0)) { }
  floatx(const floatx &X) = default;

  explicit operator M() const
  {
    int e1 = e;
    if (e1 != e)
    {
      // exponent overflow
      // assume base type M has smaller exponent range than int
      // and return appropriate over/underflow
      if (e > 0)
      {
        return m / 0; // infinity with same sign as m
      }
      else
      {
        return m * 0; // zero with same sign as m
      }
    }
    else
    {
      return ldexp(m, e1);
    }
  }
};

// needed for addition and comparisons

template<typename M, typename E>
std::tuple<M, M, E> align(const floatx<M,E> a, const floatx<M,E> b)
{
  if (a.e > b.e)
  {
    // FIXME what if b.e - a.e > INT_MAX
    return std::tuple<M, M, E>(a.m, ldexp(b.m, b.e - a.e), a.e);
  }
  else if (a.e < b.e)
  {
    // FIXME what if a.e - b.e > INT_MAX
    return std::tuple<M, M, E>(ldexp(a.m, a.e - b.e), b.m, b.e);
  }
  else
  {
    return std::tuple<M, M, E>(a.m, b.m, a.e);
  }
}

// arithmetic

template<typename M, typename E>
floatx<M, E> operator*(const floatx<M, E> &a, const floatx<M, E> &b)
{
  return floatx<M, E>(a.m * b.m, a.e + b.e);
}

template<typename M, typename E>
floatx<M, E> operator/(const floatx<M, E> &a, const floatx<M, E> &b)
{
  return floatx<M, E>(a.m / b.m, a.e - b.e);
}

template<typename M, typename E>
floatx<M, E> operator+(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return floatx<M, E>(am + bm, e);
}

template<typename M, typename E>
floatx<M, E> operator-(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return floatx<M, E>(am - bm, e);
}

template<typename M, typename E>
floatx<M, E> operator-(const floatx<M, E> &a)
{
  // FIXME does redundant normalization
  return floatx(-a.m, a.e);
}

// comparisons

template<typename M, typename E>
bool operator>(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return am > bm;
}

template<typename M, typename E>
bool operator>=(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return am >= bm;
}

template<typename M, typename E>
bool operator<(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return am < bm;
}

template<typename M, typename E>
bool operator<=(const floatx<M, E> &a, const floatx<M, E> &b)
{
  const auto&[ am, bm, e ] = align(a, b);
  return am <= bm;
}

template<typename M, typename E>
bool operator==(const floatx<M, E> &a, const floatx<M, E> &b)
{
  return a.m == b.m && a.e == b.e;
}

template<typename M, typename E>
bool operator!=(const floatx<M, E> &a, const floatx<M, E> &b)
{
  return a.m != b.m || a.e != b.e;
}

// 

template <int D, typename R>
struct hdual
{
  R x;
  R d[D];
  R dd[D][D]; // only uses [i][j] with i <= j

  // constant
  hdual(const R &x0)
  : x(x0)
  {
    for (int i = 0; i < D; ++i)
    {
      d[i] = 0;
      for (int j = i; j < D; ++j)
      {
        dd[i][j] = 0;
      }
    }
  }

  // variable
  hdual(const R &x0, int dim)
  : hdual(x0)
  {
    assert(0 <= dim);
    assert(dim < D);
    d[dim] = 1;
  }
};

// arithmetic

template <int D, typename R>
hdual<D, R> operator+(const hdual<D, R> &a, const hdual<D, R> &b)
{
  hdual<D, R> z;
  z.x = a.x + b.x;
  for (int i = 0; i < D; ++i)
  {
    z.d[i] = a.d[i] + b.d[i];
    for (int j = i; j < D; ++j)
    {
      z.dd[i][j] = a.d[i][j] + b.d[i][j];
    }
  }
  return z;
}

template <int D, typename R>
hdual<D, R> operator-(const hdual<D, R> &a, const hdual<D, R> &b)
{
  hdual<D, R> z;
  z.x = a.x - b.x;
  for (int i = 0; i < D; ++i)
  {
    z.d[i] = a.d[i] - b.d[i];
    for (int j = i; j < D; ++j)
    {
      z.dd[i][j] = a.d[i][j] - b.d[i][j];
    }
  }
  return z;
}

template <int D, typename R>
hdual<D, R> operator-(const hdual<D, R> &a)
{
  hdual<D, R> z;
  z.x = -a.x;
  for (int i = 0; i < D; ++i)
  {
    z.d[i] = -a.d[i];
    for (int j = i; j < D; ++j)
    {
      z.dd[i][j] = -a.d[i][j];
    }
  }
  return z;
}

template <int D, typename R>
hdual<D, R> operator*(const hdual<D, R> &a, const hdual<D, R> &b)
{
  hdual<D, R> z;
  z.x = a.x * b.x;
  for (int i = 0; i < D; ++i)
  {
    z.d[i] = a.x * b.d[i] + a.d[i] * b.x;
    z.dd[i][i] = a.x * b.dd[i][i] + a.d[i] * b.d[i] + a.dd[i][i] * b.x;
    for (int j = i + 1; j < D; ++j)
    {
      z.dd[i][j] = a.x * b.dd[i][j] + a.d[i] * b.d[j] + a.d[j] * b.d[i] + a.dd[i][j] * b.x;
    }
  }
  return z;
}
