# FractalTracer GLSL Edition

OpenGL Shader Language implementation of distance estimation.

Implemented for FragM environment: <https://github.com/3Dickulus/FragM>

Either configure FragM to add `/path/to/de/glsl/include` to its
search paths, or copy the contents of the `glsl/examples` folder
to inside the `glsl/include` folder (no subdirectories).

Load the `Mandelbulb-vs-Menger.frag` example: beware:
on my system it takes over 15mins to compile without
hard shadows, 50mins with hard shadows.

Tour of the repository:

- README.md:
  This file.

- COPYING.md:
  License.

- glsl:
  Implementation in OpenGL Shading Language

  - glsl/examples:

    - Mandelbulb-vs-Menger.frag:
      Example using FragM's default DE-Raytracer.frag.

  - glsl/include:

    - Common.frag:
      Meta-include file; clients just need this.

    - Builtin.frag, Overload.frag:
      GLSL overloading is weird, see comments in the
      first file.

    - Double.frag:
      GLSL implementations of double precision maths,
      mostly copy/pasted from FragM's version but with
      some fixes and cleanups here and there.

    - Real.frag, RealBase.frag:
      GLSL doesn't support overloaded operators, so
      define our own `add()`, `mul()`, etc.

    - Int.frag:
      As above but for integral types

    - FloatX.frag, FloatXBase.frag:
      Float with an int for extended exponent range.
      Needed because we need Very Large numbers.

    - Dual.frag, DualBase.frag:
      Dual numbers with one real part and multiple dual
      parts for automatic differentiation in multiple
      dimensions.

    - Vec.frag, VecBase.frag:
      Generalized low-dimensional vectors (for example,
      the fractals mainly need the vec3 of dual3 float(x)).

    - Triplex.frag, TriplexBase.frag:
      The triplex 3D number type as used in the
      Mandelbulb fractal (positive Z variant).

    - LambertW.frag, LambertWBase.frag:
      Lambert's W function aka product log, copy/pasted
      from the GNU Scientific Library version 2.6 and
      adapted to GLSL.

    - DistanceEstimate.frag:
      The fractal iterator with distance estimation code.
      Has various `uniform` variables to adjust behaviour.

    - Mandelbulb.frag:
      The Mandelbulb fractal formula, in both trigonometric
      and power 8 via repeated squaring variants.

    - MengerSponge.frag:
      The Menger sponge fractal formula, as copy/pasted
      from mandelbulber2 source code which attributes it
      to knighty.

