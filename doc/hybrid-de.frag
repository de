#version 330 compatibility
#info Distance Estimation for Hybrid Escape Time Fractals (c) Claude Heiland-Allen 2020
#include "Trudy-sRGB.frag"
#include "Trudy.frag"
#include "Complex.frag"
uint hash(uint a) { return hash_burtle_9(a); }

#group Hybrid
uniform int Iterations; slider[0,100,10000]
uniform float LogEscapeRadius; slider[1,10,100]
uniform bool EstimateGrowth; checkbox[false]
uniform bool JacobianDE; checkbox[true]
uniform bool Mandelbrot; checkbox[true]
uniform int MandelbrotPower; slider[1,2,16]
uniform bool Sierpinski; checkbox[true]
uniform float SierpinskiScale; slider[1,3,16]

float EscapeRadius2 = pow(2.0, LogEscapeRadius + 1.0);

void mandelbrot(inout vec2 z, inout mat2 J, vec2 c)
{
  vec2 dz = cPow(z, MandelbrotPower - 1);
  z = cMul(dz, z) + c;
  dz *= MandelbrotPower;
  mat2 dJ = mat2(dz.x, dz.y, -dz.y, dz.x);
  mat2 I = mat2(1.0, 0.0, 0.0, 1.0);
  J = dJ * J + I;
}

// after knighty
void sierpinski(inout vec2 z, inout mat2 J)
{
  vec2 dz = sign(z);
  mat2 dJ = mat2(dz.x, 0.0, 0.0, dz.y);
  z = abs(z);
  if (z.x - z.y < 0.0)
  {
    // swap
    float t = z.x; z.x = z.y; z.y = t;
    vec2 dt = dJ[0]; dJ[0] = dJ[1]; dJ[1] = dt;
  }
  z *= SierpinskiScale;
  dJ *= SierpinskiScale;
  z.x -= SierpinskiScale - 1.0;
  if (z.y > 1.0) z.y -= SierpinskiScale - 1.0;
  J = transpose(dJ) * J;
}

// hybrid
void formula(inout vec2 z, inout mat2 J, vec2 c)
{
  if (Mandelbrot) mandelbrot(z, J, c);
  if (Sierpinski) sierpinski(z, J);
}

// returns df, mu
vec2 iterate(vec2 q)
{
  vec2 c = q;
  vec2 z = Mandelbrot ? vec2(0.0) : c;
  mat2 J = mat2(1.0, 0.0, 0.0, 1.0);
  int n;
  for (n = 0; n < Iterations; ++n)
  {
    if (dot(z, z) > EscapeRadius2) break;
    formula(z, J, c);
  }
  if (! (dot(z, z) > EscapeRadius2))
    return vec2(1.0 / 0.0); // infinity
  float z0 = length(z);
  float dz0 = length(normalize(z) * J);
  float  p, a;
  if (EstimateGrowth)
  {
    formula(z, J, c);
    float z1 = length(z);
    formula(z, J, c);
    float  z2 = length(z);
    p = (log(z2) - log(z1)) / (log(z1) - log(z0));
    a = log(z1) - p * log(z0);
  }
  else
  {
    p = MandelbrotPower;
    a = SierpinskiScale;
  }
  float R = sqrt(EscapeRadius2);
  float f, df;
  if (a == 1)
  {
    f = log(log(z0)) / log(p);
    df = dz0 / (z0 * log(z0) * log(p));
  }
  else if (p == 1)
  {
    f = log(z0) / log(a);
    df = dz0 / (z0 * log(a));
  }
  else
  {
    f = log(log(a) + (p - 1.0) * log(z0)) / log(p);
    df = dz0 * (p - 1.0) / (z0 * log(p) * (log(a) + (p - 1.0) * log(z0)));
  }
  return vec2(df / p, float(n) - f);
}

vec3 color(vec2 q, vec2 dx, vec2 dy)
{
  float de;
  if (JacobianDE)
  {
    float pixel_spacing = length(vec4(dx, dy));
    de = 1.0 / (iterate(q).x * pixel_spacing);
  }
  else
  {
    float factor = 1.0 / 256.0;
    dx *= factor;
    dy *= factor;
    float n00 = iterate(q - dx - dy).y;
    float n01 = iterate(q - dx + dy).y;
    float n10 = iterate(q + dx - dy).y;
    float n11 = iterate(q + dx + dy).y;
    de = factor / length(vec2(n01 - n10, n00 - n11));
  }
  if (isnan(de) || isinf(de))
    return vec3(0.0);
  return vec3(tanh(float(clamp(de, 0.0, 4.0))));
}


#preset Sierpinski
Iterations = 100
EstimateGrowth = false
JacobianDE = true
Mandelbrot = false
Sierpinski = true
SierpinskiScale = 3
Zoom = 1
Center = 0,0
#endpreset


#preset Mandelbrot
Iterations = 100
EstimateGrowth = false
JacobianDE = true
Mandelbrot = true
MandelbrotPower = 2
Sierpinski = false
Zoom = 0.8
Center = -0.75,0
#endpreset


#preset Hybrid
Iterations = 100
EstimateGrowth = false
JacobianDE = true
Mandelbrot = true
MandelbrotPower = 2
Sierpinski = true
SierpinskiScale = 3
Zoom = 0.8
Center = -0.125,0
#endpreset
