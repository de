# to log-spherical
T(x, y, z) = ( log(sqrt(x^2+y^2+z^2)), atan2(y,x), acos(z/sqrt(x^2+y^2+z^2)) );
# scaling by n
N(n, l, theta, phi) = ( n * l, n * theta, n * phi );
# from log-spherical
S(l, theta, phi) = ( exp(l)*cos(theta)*sin(phi), exp(l)*sin(theta)*sin(phi), exp(l)*cos(phi) );
# compute Jacobian
n = var("n")
x = var("x");
y = var("y");
z = var("z");
assume(n, 'integer');
assume(n > 0);
J = jacobian(S(*N(n, *T(x, y, z))), (x, y, z));
# compute Jacobian determinant
Jdet = J.det().simplify_trig().simplify();
# pretty print
r = var("r");
assume(r > 0);
phi = var("phi");
theta = var("theta");
print (Jdet.subs({x^2+y^2+z^2: r^2, arccos(z/r): phi}).simplify_trig());
#print(J);
#J = J.subs({x^2+y^2+z^2: r^2, arccos(z/r): phi, arctan2(y, x): theta}).apply_map(lambda x: x.expand().expand().expand().full_simplify());
#print(J);

J = J.subs({n: 2, x: 1, y: 0 }).simplify();
#print(J)
#var("m");
#assume(m, 'integer');
e = var("e");
assume(e > 0);
J = J.apply_map(lambda l: limit(l, z = e, dir='plus'));
es = sorted(map(abs, J.eigenvalues()));
print (es)
c = es[2] / es[0];
print (c)
es = list(map(lambda l: limit(l, e = 1/100, dir='plus'), es));
print (es)
c = es[2] / es[0];
print (c)
print (c.n(100))

J = J.apply_map(lambda l: limit(l, e = 1/100, dir='plus'));
vs = J.eigenvectors_right();
print (vs)

if false:
  max_c = 0;
  max_v = {};
  with seed(1):
    for t in range(1, 100):
      v = {n: 2, x: normalvariate(0, 1e-3), y: 0, z: normalvariate(0, 2)};
      J2 = J.subs(v).apply_map(lambda l: RDF(l));
      e = sorted(map(abs, J2.n().eigenvalues()));
      c = e[2] / e[0];
      if c > max_c:
        max_c = c;
        max_v = v;
  
  #print (max_c, max_v);
  
