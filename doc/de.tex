\documentclass[a4paper]{article}
\usepackage[margin=3cm]{geometry}
\usepackage{hyperref}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{MnSymbol}
\usepackage{tikz-cd}
\usepackage{listings}

\title{Distance Estimation}
\author{Claude Heiland-Allen}
\date{2020-04-27}

\lstset{
  breaklines=true,
  breakatwhitespace=true,
  prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rhookswarrow}},
  postbreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\rcurvearrowse\space}}
}


\begin{document}
\maketitle

\section{Complex Dynamics}

\subsection{Polynomial Julia Sets}

For points in the Mandelbrot set $c \in M$, the filled-in Julia set
$K_c$ of the polynomial $f_c(z) = z^p+c$ is connected and there exists a
biholomorphic function $\phi_c$ from the complement of the filled-in
Julia set to the complement of the closed unit disk $D$ such that
$f_0 \circ \phi_c = \phi_c \circ f_c$, i.e. the dynamics are conjugated.
For $z \sim \infty$, $\phi_c(z) \sim z$, and $|\phi_c(z)| \to 1$ as
$z \to K_c$.

$g_0(z) = \log|z|$ is a potential function for the unit disk, and
$g_c(z) = \log|\phi_c(z)|$ is a potential function for $K_c$.  Define
$e_c(z) = -\frac{\log g_c(z)}{\log p}$ as a continuous escape time
function.  For disconnected $K_c$, if $z$ is large enough then the
contours are circle-like and $\phi_c(z)$ is still defined; in particular
$\phi_c(c)$ is defined for $c \not\in M$.

\begin{figure}[!ht]
\centering
\begin{tikzcd}[row sep=large,every cell/.append style={font=\Large},every label/.append style={font=\large}]
D\setminus\{-w\} \arrow[rr, "\begin{matrix} \text{restrict} \\ \supset \end{matrix}"] & & {|w|D} \\
& & \\
D\setminus\{0\} \arrow[uu, "m_w : z \mapsto \frac{z - w}{1 - \bar{w} z}"] & & D \arrow[uu, "s_{|w|} : z \mapsto |w| z"'] \arrow[dd, "{\psi:d(z_0,K_c)>\frac{1}{4}|\psi'(0)|}"] \\
& & \\
\mathbb{C}\setminus D \arrow[uu, "r : z \mapsto \frac{1}{z}"] \arrow[uurr, "\theta_w"] \arrow[dd, "f_0 : z \mapsto z^p"'] & & \mathbb{C}\setminus K_c \arrow[ll, "\phi_c"'] \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\mathbb{C}\setminus D \arrow[dd, "f_0 : z \mapsto z^p"'] & & \mathbb{C}\setminus K_c \arrow[ll, "\phi_c"'] \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\vdots \arrow[dd, "f_0 : z \mapsto z^p"'] & & \vdots \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\mathbb{C}\setminus D \arrow[ddr, "g_0 : G = \log|z_0|"'] & & \mathbb{C}\setminus K_c \arrow[ll, "\phi_c \sim \operatorname{id}"'] \arrow[ddl, "g_c : G = \lim_{k\to\infty} \frac{\log|z_k|}{p^k}"] \\
& & \\
& \mathbb{R}^+ \arrow[ddl, "e : G \mapsto -\frac{\log G}{\log p}"'] \arrow[ddr, "d : G \mapsto \frac{\sinh G}{2 G' \exp G}"] & \\
& & \\
e_c(z_0)\in\mathbb{R} & & d_-(z_0,K_c)\in\mathbb{R}^+
\end{tikzcd}
\caption{Commutative diagram for continuous escape time and distance estimate via potential.
The lower bound for the distance from a point to the filled in Julia
set is calculated using the Koebe $\frac{1}{4}$ Theorem applied to
$\psi$: $d(z_0, K_c) > \frac{1}{4} |\psi'(0)|$. $w = \frac{1}{\phi_c(z_0)}$
is chosen in the Möbius transformation $m_w$ to move the puncture out
of the way.  See Yuval Fisher's {\em{Appendix D}} in {\em{The Science Of Fractal
Images}} by Heinz-Otto Peitgen and Dietmar Saupe (editors).}
\label{fig:complex}
\end{figure}

A useful representation of the potential is
$$g_c(z) = \lim_{k\to\infty} \frac{\log|f_c^k(z)|}{p^k}$$
Calculating:
\begingroup
\allowdisplaybreaks
\begin{align*}
 & g(f_c(z)) \\
= & \text{\{ definition of $g_c$ \}} \\
 & \log|\phi_c(f_c(z))| \\
= & \text{\{ definition of $\phi_c$ \}} \\
 & \log|f_0(\phi_c(z))| \\
= & \text{\{ definition of $f_0$ \}} \\
 & \log|\phi_c(z)^p| \\
= & \text{\{ $|\cdot|$ commutes with $\cdot^p$ \}} \\
 & \log|\phi_c(z)|^p \\
= & \text{\{ $\log$ of a power \}} \\
 & p\log|\phi_c(z)| \\
= & \text{\{ definition of $g_c$ \}} \\
 & p g_c(z) \\
\implies & \\
 & g_c(z) \\
= & \text{\{ iteration \}} \\
 & \lim_{k\to\infty} \frac{g_c(f_c^k(z))}{p^k} \\
= & \text{\{ definition of $g_c$ \}} \\
 & \lim_{k\to\infty} \frac{\log|\phi_c(f_c^k(z))|}{p^k} \\
= & \text{\{ $k\to \infty \implies f_c^k(z) \to \infty \implies \phi_c(f_c^k(z)) \sim f_c^k(z)$ \}} \\
 & \lim_{k\to\infty} \frac{\log|f_c^k(z)|}{p^k} \\
\end{align*}
\endgroup

Calculating:
\begingroup
\allowdisplaybreaks
\begin{align*}
 & d(z_0, K_c) \\
> & \text{\{ Koebe $\frac{1}{4}$ Theorem \}} \\
 & \frac{1}{4} |\psi'(0)| \\
= & \text{\{ definition of $\psi$ \}} \\
 & \frac{1}{4} |((m_w \circ r \circ \phi_c)^{-1} \circ s_{|w|})'(0)| \\
= & \text{\{ algebra \}} \\
 & \frac{1}{4} |(\phi_c^{-1} \circ r^{-1} \circ m_w^{-1} \circ s_{|w|})'(0)| \\
= & \text{\{ chain rule \}} \\
 & \frac{1}{4} |\left(\phi_c^{-1}\right)'\left(\frac{1}{w}\right)
    \cdot \left(r^{-1}\right)'(w)
    \cdot \left(m_w^{-1}\right)'(0)
    \cdot \left(s_{|w|}\right)'(0)| \\
= & \text{\{ $\left(F^{-1}(z)\right)' = \left(F'(z)\right)^{-1}$ \}} \\
 & \frac{1}{4} \left| \frac{1}{\phi_c'(z_0)} \frac{-1}{w^2} \frac{1}{m_w'(w)} |w| \right| \\
= & \text{\{ algebra \}} \\
 & \frac{1}{4} \left| \frac{1}{\phi_c'(z_0)} \frac{1}{w} \frac{1}{\frac{1 - w \bar{w}}{\left(1 - w \bar{w}\right)^2}} \right| \\
= & \text{\{ algebra \}} \\
 & \frac{1}{4} \left| \frac{1}{\phi_c'(z_0)} \frac{1 - |w|^2}{w} \right| \\
= & \text{\{ $w = \frac{1}{\phi_c(z_0)}$ \}} \\
 & \frac{1}{4} \left| \frac{1}{\phi_c'(z_0)} \frac{1 - |\frac{1}{\phi_c(z_0)}|^2}{\frac{1}{\phi_c(z_0)}} \right| \\
= & \text{\{ multiply top and bottom by $\phi_c(z_0)$ \}} \\
 & \frac{1}{4} \frac{||\phi_c(z_0)|^2 - 1|}{|\phi_c(z_0)| |\phi_c'(z_0)|} \\
= & \text{\{ $|\phi_c(z_0)| > 1$ \}} \\
 & \frac{1}{4} \frac{|\phi_c(z_0)|^2 - 1}{|\phi_c(z_0)| |\phi_c'(z_0)|} \\
= & \text{\{ substitute $g_c(z_0) = \log|\phi_c(z_0)| \implies |\phi_c(z_0)| = \exp g_c(z_0) \implies |\phi_c'(z_0)| = g_c'(z_0) \exp g_c(z_0)$ \}} \\
 & \frac{1}{4} \frac{(\exp g_c(z_0))^2 - 1}{(\exp g_c(z_0))^2 g_c'(z_0)} \\
= & \text{\{ algebra \}} \\
 & \frac{\frac{1}{2}(\exp g_c(z_0) - \exp(-g_c(z_0)))}{2 g_c'(z_0) \exp g_c(z_0)} \\
= & \text{\{ definition of $\sinh$ \}} \\
 & \frac{\sinh g_c(z_0)}{2 g_c'(z_0) \exp g_c(z_0)} \\
\end{align*}
\endgroup

\subsubsection{$z \to z^p$ on $\mathbb{C}\setminus D$}

A precise distance estimate is $d_= = |z| - 1$.  The potential is $G = \log|z|$.
The lower bound via potential theory is $d_- = \frac{\sinh G}{2 G' \exp G} = \frac{|z|^2-1}{4|z|} = (|z|-1)\left(\frac{1}{4} + \frac{1}{4|z|}\right) < |z|-1 = d_=$ when $z > 1$,
so the distance estimate is a consistent lower bound.  In fact $\frac{1}{4} < \frac{d_-}{d_=} < \frac{1}{2}$, which gives an idea of the underestimation of the distance from potential methods.

\subsubsection{$z \to a z^p$ on $\mathbb{C}\setminus \frac{D}{\sqrt[p-1]{a}}$}

$z_k = a^\frac{p^k - 1}{p - 1} z_0^{p^k}$ ({\em{Ueber Iterirte Functionen}}, Schröder 1870).
$z' \to a p z^{p-1} z'$ so $z_k' = a^\frac{p^k + k (p - 2) (p-1) - 1}{(p-1)^2} p^k z^{p^k-1}$
The potential is $G = \log|\sqrt[p-1]{a} z|$
A precise distance estimate is $d_= |z_0| - \frac{1}{\sqrt[p-1]{a}}$.
The lower bound via potential theory is $d_- = \frac{\sinh G}{2 G' \exp G} = \frac{\frac{1}{2}\left(\sqrt[p-1]{a} |z| - \frac{1}{\sqrt[p-1]{a} |z|}\right)}{2 \sqrt[p-1]{a} |z| \frac{1}{|z|}} = \frac{|z|}{4} - \frac{1}{4 \sqrt[p-1]{a}^2 |z|} < d_=$ when $z > \frac{1}{\sqrt[p-1]{a}}$.
Suppose $|z_0| = \frac{1 + \epsilon}{\sqrt[p-1]{a}}$.  Then $d_- = \frac{1 + \epsilon}{4 \sqrt[p-1]{a}} - \frac{1}{4\sqrt[p-1]{a}(1 + \epsilon)} = \frac{(1 + \epsilon)^2 - 1}{4 \sqrt[p-1]{a}(1 + \epsilon)} = \frac{\epsilon^2 + 2 \epsilon + 1 - 1}{4 \sqrt[p-1]{a} (1 + \epsilon)} = \frac{\epsilon(\epsilon+2)}{4\sqrt[p-1]{a}(\epsilon+1)}$.
Now $\frac{1}{4} < \frac{d_-}{d_=} = \frac{\epsilon + 2}{4 (\epsilon + 1)} < \frac{1}{2}$ so the lower bound is not a trivial one.

\subsubsection{$z \to z^p + c$ on $\mathbb{C}\setminus K_c$}

A precise distance estimate may be achievable for some specific $c$
values (exercise: try $c = -2$).  The potential is
$G = \lim \frac{\log|z_k|}{p^k}$.
Now $d_- = \frac{\sinh G}{2 G' \exp G} > \frac{2}{2 G' \exp G} = \lim_{k \to \infty}\frac{|z_k| \log |z_k|}{2|z_k|^\frac{1}{p^k} |z_k'|}$,
using the fact that $\sinh x > x$ for all $x > 0$.

\subsubsection{$z \to a z^p + c$ on $\mathbb{C}\setminus \frac{K_\frac{c}{\sqrt[p-1]{a}}}{\sqrt[p-1]{a}}$}

The filled-in Julia set of $z \to a z^p + c$ is a scaled copy of the filled
in Julia set for $z \to z^p + c \sqrt[p-1]{a}$.  So the distance is scaled too:
$d(\frac{z}{\sqrt[p-1]{a}},\frac{K_\frac{c}{\sqrt[p-1]{a}}}{\sqrt[p-1]{a}}) = \frac{d(z, K_c)}{\sqrt[p-1]{a}}$.

\subsection{Polynomial Mandelbrot Sets}

Define $\Phi : \mathbb{C}\setminus M \to \mathbb{C}\setminus D$,
with $\Phi(c) = \phi_c(c)$ then a similar potential argument with
$G(c) = \log|\Phi(c)| = \lim_{k \to \infty}\frac{\log|z_k|}{p^k}$
with $z_0 = c$ gives
\begingroup
\allowdisplaybreaks
\begin{align*}
& d(c, M) \\
> & \text{\{ potential theory \}} \\
& \frac{\sinh G(c)}{2 G'(c) \exp G(c)} \\
> & \text{\{ $\sinh x > x$ for $x > 0$ \}} \\
& \lim_{k \to \infty} \frac{|z_k| \log |z_k|}{2 |z_k|^\frac{1}{p^k} |z_k'|} \\
\end{align*}
\endgroup

\subsection{Escape Time}

Figure~\ref{fig:complex} shows the definition of continuous escape
time, derived from the potential via $e = -\frac{\log G}{\log p}$.
Calculating:
\begingroup
\allowdisplaybreaks
\begin{align*}
G &= \exp(-e\log p) \\
G' &= -e'\log p \exp(-e \log p) \\
  &= -G e' \log p \\
\frac{G}{|G'|} &= \frac{1}{|e'|\log p} \\
  & d(z_0, K_c) \text{ or } d(c, M) \\
> & \text{\{ potential theory \}} \\
  & \frac{\sinh G}{2 G' \exp G} \\
> & \text{\{ $\sinh x > x$ for $x > 0$ \}} \\
  & \frac{G}{2 G' \exp G} \\
= & \text{\{ substitute $G = \exp (-e \log p)$ \}} \\
  & \frac{1}{2 |e'| \log p \exp \exp(-e \log p)} \\
= & \text{\{ $e \to \infty$ as $z_0 \to K_c$ or $z_0 = c \to M$ \}} \\
  & \frac{1}{(2 + \epsilon) |e'| \log p} \\
\end{align*}
where $\epsilon \to 0$ very rapidly as $z_0$ approaches the fractal.
Moreover:
\begingroup
\allowdisplaybreaks
\begin{align*}
  & e \\
= & \text{\{ definition of $e$ \}} \\
  & - \frac{\log G}{\log p} \\
= & \text{\{ definition of $G$ \}} \\
  & - \frac{\log \lim_{k\to\infty}\frac{\log|z_k|}{p^k}}{\log p} \\
= & \text{\{ commute $\lim$ with $\log$ \}} \\
  & - \frac{\lim_{k\to\infty}\log \frac{\log|z_k|}{p^k}}{\log p} \\
= & \text{\{ commute $\lim$ with $\frac{\cdot}{\cdot}$ \}} \\
  & - \lim_{k\to\infty}\frac{\log \frac{\log|z_k|}{p^k}}{\log p} \\
= & \text{\{ commute $\lim$ with $-\cdot$ \}} \\
  & \lim_{k\to\infty}-\frac{\log \frac{\log|z_k|}{p^k}}{\log p} \\
= & \text{\{ $\log\frac{a}{b} = \log a - \log b$ \}} \\
  & \lim_{k\to\infty}-\frac{\log \log|z_k| - \log{p^k}}{\log p} \\
= & \text{\{ $\log\frac{a^b} = b\log a$ \}} \\
  & \lim_{k\to\infty}-\frac{\log \log|z_k| - k \log p}{\log p} \\
= & \text{\{ algebra \}} \\
  & \lim_{k\to\infty}\frac{k \log p - \log \log|z_k|}{\log p} \\
= & \text{\{ algebra \}} \\
  & \lim_{k \to\infty} k - \frac{\log \log |z_k| }{\log p} \\
\end{align*}
\endgroup
and:
\begingroup
\allowdisplaybreaks
\begin{align*}
  & e' \\
= & \text{\{ calculation above \}} \\
  & \left(\lim_{k \to\infty} k - \frac{\log \log |z_k| }{\log p}\right)' \\
= & \text{\{ commute $\lim$ with $\cdot'$ \}} \\
  & \lim_{k \to\infty} \left(k - \frac{\log \log |z_k| }{\log p}\right)' \\
= & \text{\{ linearity of $\cdot'$ \}} \\
  & \lim_{k \to\infty} k' - \frac{\left(\log \log |z_k|\right)'}{\log p} \\
= & \text{\{ $(\log\log x)' = \frac{x'}{x \log x}$ \}} \\
  & \lim_{k \to\infty} k' - \frac{|z_k'|}{|z_k| \log |z_k| \log p} \\
= & \text{\{ $k$ is piecewise constant, so $k' = 0$ almost everywhere \}} \\
  & \lim_{k \to\infty} - \frac{|z_k'|}{|z_k| \log |z_k| \log p} \\
\end{align*}
\endgroup
therefore
\begingroup
\allowdisplaybreaks
\begin{align*}
  & d(z_0, K_c) \text{ or } d(c, M) \\
> & \text{\{ calculations above \}} \\
  & \frac{1}{(2 + \epsilon) |e'| \log p} \\
= & \text{\{ calculation above \}} \\
  & \frac{|z_k| \log |z_k|}{(2 + \epsilon) |z_k'|} \\
\end{align*}
\endgroup

\subsection{Hypothesis}

The derivation of
$$ d > \frac{1}{(2 + \epsilon) |e'| \log p} = \frac{|z_k| \log |z_k|}{(2 + \epsilon) |z_k'|} $$
depends on a biholomorphic $\phi$ that conjugates complex iterations outside
the 2D fractal with simple iterations outside the unit disc.

If one were to construct a $Q$-quasiconformal (note: using $Q$ instead
of the traditional $K$ to avoid aliasing with the filled-in Julia set $K_c$)
$\phi$ that conjugates iterations
outside 3D (or more generally nD) fractal with simple iterations outside
the unit ball, the machinery of the proof of the lower bound could be ported
using Astala and Gehring's analogue of the
Koebe $\frac{1}{4}$ Theorem
(rotations and scalings are trivial,
replacing complex reciprocal by sphere inversion,
all are specialized cases of
Möbius transformations in $\mathbb{R}^n$).
Only the constant $\frac{1}{4}$
would differ, becoming $\frac{1}{q}$
depending on the quasiconformal constant $Q$ of the
constructed $\phi$ and the dimension of the space $n$.

Ref: \url{https://en.wikipedia.org/wiki/M\%C3\%B6bius_transformation\#Higher_dimensions}

\section{Three Dimensions}

\subsection{The Menger Sponge}

knighty's Kaleidoscopic Escape Time IFS
\url{http://www.fractalforums.com/sierpinski-gasket/kaleidoscopic-(escape-time-ifs)/}
\begin{lstlisting}
Menger3(x,y,z){
   r=x*x+y*y+z*z;
   for(i=0;i<MI && r<bailout;i++){
      rotate1(x,y,z);

      x=abs(x);y=abs(y);z=abs(z);
      if(x-y<0){x1=y;y=x;x=x1;}
      if(x-z<0){x1=z;z=x;x=x1;}
      if(y-z<0){y1=z;z=y;y=y1;}

      rotate2(x,y,z);

      x=scale*x-CX*(scale-1);
      y=scale*y-CY*(scale-1);
      z=scale*z;
      if(z>0.5*CZ*(scale-1)) z-=CZ*(scale-1);

      r=x*x+y*y+z*z;
   }
   return (sqrt(x*x+y*y+z*z)-2)*scale^(-i);
}
\end{lstlisting}

which can be optimized to the hardcoded

\begin{lstlisting}[language=C]
vec3 sort(vec3 w)
{
  float mi = min(min(w.x, w.y), w.z);
  float ma = max(max(w.x, w.y), w.z);
  float md = w.x + w.y + w.z - mi - ma;
  return vec3(ma, md, mi);
}

vec3 step(vec3 w)
{
  w = abs(w);
  w = sort(w);
  w = 3.0 * w;
  w = w - vec3(2.0, 2.0, 2.0 * float(w.z > 1.0));
  return w;
}
\end{lstlisting}

For a generic point $w = \begin{pmatrix} x & y & z \end{pmatrix}^T$
in the complement of the Menger sponge,
not exactly on any of the $|\cdot|$ folding planes which are a set of (3-dimensional) measure 0 (TODO prove this),
the step function is $Q$-quasiconformal with $Q=1$ because it stretches in all
directions uniformly by a factor of $3$.
Thus the composition of steps until $w$ becomes large is also $1$-quasiconformal.

Following Astala and Gehring, the Jacobian of each step is a permutation
of $\begin{pmatrix} \pm 3 & 0 & 0 \\ 0 & \pm 3 & 0 \\ 0 & 0 & \pm 3 \end{pmatrix}$
so the Jacobian determinant $J_f = 3^n$.  Then
$(\log J_f)_B = \frac{1}{m(B)} \int_B \log J_f \mathrm{d}m = \log J_f$
for each ball $B$, and for each $x$
$a_f(x) = \exp\left(\frac{1}{n}(\log J_f)_{B(x)}\right) = \sqrt[n]{J_f} = 3$

Let $g : R^n \setminus M \to B(0, \sqrt{n})$
Now by Theorem 1.8:

$$d(f(x), \partial D') >= \frac{1}{q} a_f(x) d(x, \partial D)$$
where $q$ depends only on $Q = 1$ and $n = 3$.

Note: this may be dodgy because $\phi$ conjugates
the complement of the Menger sponge with
the complement of the unit cube, not the complement of the unit ball.
TODO investigate.

\subsection{The Mandelbulb}

One variant of the Mandelbulb is defined by
$$f_c \begin{pmatrix}x \\ y \\ z\end{pmatrix} = r^n \begin{pmatrix} \cos n\theta \cos n\phi \\ \sin n\theta \cos n\phi \\ \phantom{\cos n\theta} \sin n\phi \end{pmatrix} + c$$
where
$r = \sqrt{x^2 + y^2 + z^2}$, $\theta = \tan^{-1}(x,y)$, $\phi = \sin^{-1} \frac{z}{r}$
where $\tan^{-1}(x, y)$ is as implemented by the C function {\tt{atan2(y,x)}}.
Ref: \url{http://www.bugman123.com/Hypercomplex/\#MandelbulbZ}
Calculating the Jacobian determinant of $f_c$ gives
$$\frac{n^3 r^{3 (n - 1)} \cos(n \phi) r}{\sqrt{x^2 + y^2}}$$
which shows the Juliabulb (with $c$ constant) iteration function is singular when
$(x,y) = 0$ or $\cos(n\phi) = 0$ (for example when $\phi = \frac{\pi}{2n})$).
This is a bad bulb variation.

The non-``alternate'' version
in {\em FragM}'s default {\tt Mandelbulb.frag}) is defined by:
$$f_c \begin{pmatrix}x \\ y \\ z\end{pmatrix} = r^n \begin{pmatrix} \cos(n\theta)\sin(n\phi) \\ \sin(n\theta)\sin(n\phi) \\ \phantom{\sin(n\theta)} \cos(n\phi) \end{pmatrix} + c$$
where
$r = \sqrt{x^2 + y^2 + z^2}$, $\theta = \tan^{-1}(x,y)$, $\phi = \cos^{-1} \frac{z}{r}$
where $\tan^{-1}(x, y)$ is as implemented by the C function {\tt{atan2(y,x)}}.
The difference is in the definition of $\phi$ using $\cos$ instead of $\sin$,
which makes the Jacobian determinant of $f_c$ work out to
$$\frac{n^3 r^{3 (n - 1)} \sin(n \phi) r}{\sqrt{x^2 + y^2}}$$
Evaluating the limit at the potentially problematic $(x,y) = 0$ gives
$$n^4 z^{3 (n - 1)}$$
for example using Maxima:
\begin{lstlisting}
limit(limit(n^3 * (x^2 + y^2 + z^2)^((3 * n - 2)/2)
  * sin(n * acos(z / sqrt(x^2 + y^2 + z^2)))
  / sqrt(x^2 + y^2), x, 0), y, 0);
\end{lstlisting}

SageMath code for the calculation:
\lstinputlisting[language=python]{mandelbulb-jdet.sage}

knighty: ``For the mandelbulb I use sometime a similar technique. Instead of $DE=0.5*r*log(r)/dr$, I use $DE=0.5*r*log(sqrt(r^2+1.5^2)-1.5)$. All this is in order to get a DE based solid threshold consistant with max iterations solid threshold.''
\url{http://www.fractalforums.com/sierpinski-gasket/kaleidoscopic-(escape-time-ifs)/msg16658/#msg16658}


\begin{figure}[!ht]
\centering
\begin{tikzcd}[row sep=large,every cell/.append style={font=\Large},every label/.append style={font=\large}]
B\setminus\{-w\} \arrow[rr, "\begin{matrix} \text{restrict} \\ \supset \end{matrix}"] & & {|w| B} \\
& & \\
B\setminus\{0\} \arrow[uu, "{m_w : z \mapsto \frac{1}{|w|} + \frac{|w|^2 - 1}{|w|^2} \frac{\left(z + \frac{1}{|w|}X\right)}{\left|z + \frac{1}{|w|}X\right|^2}}"] & & B \arrow[uu, "{s_{|w|} : z \mapsto |w| z}"'] \arrow[dd, "{\psi:d(z_0,K_c)>\frac{1}{q}|a_f(0)|}"] \\
B\setminus\{0\} \arrow[u, "{R_w : z \mapsto X_w z}"] & & \\
\mathbb{R}^n\setminus B \arrow[u, "r : z \mapsto \frac{z}{|z|^2}"] \arrow[uurr, "{\theta_w}"] \arrow[dd, "f_0 : z \mapsto z^p"'] & & \mathbb{R}^n\setminus K_c \arrow[ll, "\phi_c"'] \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\mathbb{R}^n\setminus B \arrow[dd, "f_0 : z \mapsto z^p"'] & & \mathbb{R}^n\setminus K_c \arrow[ll, "\phi_c"'] \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\vdots \arrow[dd, "f_0 : z \mapsto z^p"'] & & \vdots \arrow[dd, "f_c : z \mapsto z^p + c"] \\
& & \\
\mathbb{R}^n\setminus B \arrow[ddr, "g_0 : G = \log|z_0|"'] & & \mathbb{R}^n\setminus K_c \arrow[ll, "\phi_c \sim \operatorname{id}"'] \arrow[ddl, "g_c : G = \lim_{k\to\infty} \frac{\log|z_k|}{p^k}"] \\
& & \\
& \mathbb{R}^+ \arrow[ddl, "e : G \mapsto -\frac{\log G}{\log p}"'] \arrow[ddr, "d : G \mapsto \frac{\sinh G}{2 G' \exp G}"] & \\
& & \\
e_c(z_0)\in\mathbb{R} & & d_-(z_0,K_c)\in\mathbb{R}^+
\end{tikzcd}
\caption{Commutative diagram for continuous escape time and distance estimate via potential.
The lower bound for the distance from a point to the filled-in Julia
set is calculated using Theorem 1.8 of Astala and Gehring applied to
$\psi$: $d(z_0, K_c) > \frac{1}{q} |a_\psi(0)|$. $w = \frac{\phi_c(z_0)}{|\phi_c(z_0)^2|}$
is chosen in the Möbius transformation $m_w$ to move the puncture out
of the way.  $X_w$ is a rotation matrix that puts $w$ onto $|w|X$ where
$X = \begin{pmatrix} 1 & 0 & 0 \end{pmatrix}^T$.
Compare with the complex case in Figure~\ref{fig:complex}.}
\label{fig:realn}
\end{figure}

For the Mandelbulb Juliabulb, construct $\psi$ as in Figure~\ref{fig:realn}.
As the potential $G = \log|z|$ outside the unit ball $B$ is
$n$-harmonic (TODO precise citation), so too is the potential
outside the Mandelbulb (Juliabulb) $K_c$ because the map $\phi_c$ is
$Q$-quasiconformal with $Q = TODO$.  In particular,
Möbius transformations preservec $n$-harmony (user31373's M.SE answer),
and the upper part of Figure~\ref{fig:realn} $\theta_w : \mathbb{R}^n\setminus B \to B$
is a Möbius transformation.  It follows that $\psi$ is $Q$-quasiconformal,
because if $u$ is $U$-quasiconformal and $v$ is $V$-quasiconformal then
$u \circ v$ is $UV$-quasiconformal, and $u^{-1}$ is also $U$-quasiconformal,
with $u = \theta_w$ being a $1$-quasiconformal (conformal) Möbius transformations
and $v = \phi_c$ being $Q$-quasiconformal.

\url{https://en.wikipedia.org/wiki/Harmonic_function\#The_mean_value_property}
\section*{Bibliography}

Fisher, Y.
{\em Exploring the Mandelbrot set} \\
Appendix D in \\
Peitgen, H.-O.; Saupe, D. (editors)
{\em The Science of Fractal Images} \\
Springer-Verlag New York Inc 1988

Heinonen, J.
{\em What Is... a Quasiconformal Mapping?}
Notices of the AMS
Volume 53, Number 11, December 2006, 1334--1335.
\url{http://www.ams.org/notices/200611/whatis-heinonen.pdf}

Astala, K.; Gehring, F. W.
{\em Quasiconformal analogues of theorems of Koebe and Hardy-Littlewood} \\
Michigan Math. J. 32 (1985), no. 1, 99--107.
{\tt doi:10.1307/mmj/1029003136} \\
\url{https://projecteuclid.org/euclid.mmj/1029003136}

user31373
{\em Harmonic function composed with conformal map is harmonic (in $\mathbb{R}^n$)} \\
\url{https://math.stackexchange.com/q/151838} (version: 2017-04-13)
%@MISC {151838,
%    TITLE = {Harmonic function composed with conformal map is harmonic (in $\mathbb{R}^n$)},
%    AUTHOR = {user31373},
%    HOWPUBLISHED = {Mathematics Stack Exchange},
%    NOTE = {URL:https://math.stackexchange.com/q/151838 (version: 2017-04-13)},
%    EPRINT = {https://math.stackexchange.com/q/151838},
%    URL = {https://math.stackexchange.com/q/151838}
%}

Lindqvist, P.
{\em Notes on the p-Laplace equation} \\
\url{https://folk.ntnu.no/lqvist/p-laplace.pdf} (retrieved 2020-04-28)

Axler, S.; Bourdon, P.; Ramey, W.
{\em Harmonic Function Theory (Second Edition)} \\
\url{https://sites.math.washington.edu/~morrow/336_13/HFT.pdf} (retrieved 2020-04-28)

Heinonen, J.; Kilpelainen, T.; Martio, O.
{\em Nonlinear  Potential Theory  of  Degenerate Elliptic  Equations} \\
Dover Publications, Inc. Mineola, New York. 2006.
reprinted from
Oxford University Press, Inc., New York. 1993.

SageMath, the Sage Mathematics Software System (Version 9.0),
The Sage Developers, 2020, \\
\url{https://www.sagemath.org}
%@manual{sagemath,
%  Key          = {SageMath},
%  Author       = {{The Sage Developers}},
%  Title        = {{S}ageMath, the {S}age {M}athematics {S}oftware {S}ystem ({V}ersion 9.0)},
%  note         = {{\tt https://www.sagemath.org}},
%  Year         = {2020},
%}

Maxima, a Computer Algebra System. Version 5.43.2 (2019). \\
\url{https://maxima.sourceforge.net}
%If you use Bibtex for your citations, we recommend that you add the following entry in your bibtex database:
%@ELECTRONIC{maxima,
% author = {Maxima},
% year = {2019},
% title = {Maxima, a Computer Algebra System.  Version 5.43.2},
% address = {http://maxima.sourceforge.net/},
% url = {http://maxima.sourceforge.net/},
% owner = {maxima},
% timestamp = {2019}
%}

\end{document}
