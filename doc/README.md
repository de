To install the pandoc equation numbering filter:

    pip3 install pandoc-eqnos --user

- https://github.com/tomduck/pandoc-eqnos

Then run `make`.

To rebuild the figures using `hybrid-de.frag` you need `FragM` with `trudy`.

- https://github.com/3Dickulus/FragM
- https://code.mathr.co.uk/trudy

Target image size is 886x886px at 300dpi.
