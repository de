---
title: |
  Distance Estimation for \
  Hybrid Escape Time Fractals
author: |
  Claude Heiland-Allen \
  <claude@mathr.co.uk>
date: 2020-05-01
keywords: [dynamical systems, distance estimates, fractals]
abstract: |
  The well-known distance estimates for escape time fractals
  whose iteration behaviour near $\infty$ are like $z \to a z$
  and like $z \to z^p$ are derived via the spatial derivative of
  a smooth iteration count.  A distance estimate is derived for
  hybrid fractals with iteration behaviour like $z \to a z^p$.
papersize: a4
geometry: margin=3cm
...


# Theory

## Iteration

$$z_{n+1} = F(z_n)$$

$$z_{n+k} = F^k(z_n)$$

## Escape

A function $F$ is *escaping* when there exists an *escape radius* $R > 0$
such that if $z_n > R$ then $z_{n + k} \to \infty$ as $k \to \infty$.
The *integer escape time* $n$ is the smallest $n$ such that $z_n > R$.
The *escape rate* of $F$ is the behaviour of $F$ near infinity, for example
*geometric escape* $z \to a z$ for some factor $a$ or *exponential escape*
$z \to z^p$ for some power $p$.

## Continuous Escape Time

The size of $z_n$ at escape can be used to give a *continuous escape time*
$m = n + 1 - f$, where $0 <= f < 1$ depends on the escape rate.  When $|z_n|$
is close to $R$, $f$ is close to $0$; $f$ increases to $1$ as $|z_n|$
increases until the next dwell band transition, where $n$ decreases by $1$
(as $|z_n|$ is so large it must have escaped previously) and $f$ jumps
back to $0$.  $f$ must be chosen so that the these jumps are smooth, that is
the derivatives must match up either side of the dwell band boundary:
$$\left.\frac{\partial z_{n+1}}{\partial f}\right|_{f=0} =
  \left.\frac{\partial z_n}{\partial f}\right|_{f=1}$$ {#eq:}

## Renormalized Escape Time

> The scale-dependence on the escape radius can be (partly) removed by
> differentiating the above formula with respect to the escape radius,
> taking the limit to infinity, and then integrating the first term of
> the resulting differential equation. The result is a renormalized,
> scale-independent, quasi-integral-valued iteration count.

> -- Linas Vepstas (1997) "Renormalizing the Mandelbrot Escape"
> <https://linas.org/art-gallery/escape/escape.html>

The *renormalized continuous escape time* is written as $\mu$ in this document.

## Potential

$$ G = p^{-\mu} $$ {#eq:}

## Böttcher Map

### For quadratic Julia sets

> $\phi_c$ is a uniformizing map of the basin of attraction of infinity,
> because it conjugates $f_c(z) = z^2 + c$ on the complement of the
> filled Julia set $K_c$ to $f_0(z) = z^2$ on the complement of the
> unit disk:
> $$ \phi_c : \widehat{\mathbb{C}} \setminus K_c \to \widehat{\mathbb{C}} \setminus \overline{\mathbb{D}} $$ {#eq:}
> $$ z \mapsto \lim_{n \to \infty}\left(f_c^n(z)\right)^{2^{-n}} $$ {#eq:}
> and
> $$ \phi_c \circ f_c \phi_c^{-1} = f_0 $$ {#eq:}


> -- <https://en.wikipedia.org/wiki/External_ray#Uniformization>

### For the quadratic Mandelbrot set

> $\phi_M$ is the uniformizing map of the complement of the Mandelbrot set,
> because it conjugates the complement of the Mandelbrot set $M$ and the
> complement (exterior) of the closed unit disk:
> $$ \phi_M : \widehat{\mathbb{C}} \setminus M \to \widehat{\mathbb{C}} \setminus \overline{\mathbb{D}} $$ {#eq:}
> $$ \phi_M(c) := \phi_c(c) $$ {#eq:}
> It can be normalized so that $\frac{\phi_M(c)}{c} \to 1$ as $c \to \infty$.

> -- <https://en.wikipedia.org/wiki/External_ray#Uniformization_2>

### Connection to potential

$$ G = \log_p |\phi| $$ {#eq:}
$$ \phi = p^G$$ {#eq:}

## Distance Estimate via Koebe $\frac{1}{4}$ Theorem

> **Theorem 15** The distance from a point $z$, $z \in \mathbb{C}$,
> $z \not\in K_c$ to the Julia set $K_c$ is strictly greater than
> $\frac{\sinh G(z)}{2e^{G(z)}|G'(z)|}$. That is,
> $$d(z, K_c) > \frac{\sinh G(z)}{2e^{G(z)}|G'(z)|}.$$ {#eq:}

> -- Yumei Dang, Louis H. Kauffman and Dan Sandin (????)
> "Hypercomplex Iterations, Distance Estimation and Higher Dimensional Fractals" (page 25, PDF page 38)
> <https://www.evl.uic.edu/hypercomplex/html/book/book.pdf>

The proof is via the Koebe $\frac{1}{4}$ theorem which provides bounds
on distortion:

> $$d(z, K_c) > \frac{R}{4} = \frac{|\phi_c(z)|^2 - 1}{|\phi_c(z)| |\phi_c'(z)|}$$ {#eq:lowerbound}

(Note: the $R$ in this quote is not the escape radius.)

## Another Route to the Distance Estimate

Suppose the fractal $L$ is strictly contained in a ball of radius $r$
centered at the origin $B(0, r)$. If a point is outside the fractal,
the iterations escape, so eventually $z_n > R >> r$.  Now the distance
to the fractal is strictly greater than the distance from $z_n$ to the
near side of the ball:
$$ d(z_n, L) > d(z_n, B(0, r)) = |z_n| - r $$ {#eq:}
and the derivative $z_n'$ tells how space has been locally warped
by each iteration, so they can be undone to estimate $d(z_0, L)$.  For
linear functions, 
$$d(z_0, L) > \frac{|z_n| - r}{|z_n'|}$$ {#eq:}
but non-linear functions stretch nearby space so the distances might be
smaller or larger.

## Approximating the Distance Estimate

For points near the fractal, the continuous escape time $\mu$ is large,
which means the potential $G = \exp(-\mu)$ is small.  For all $G \ge 0$,
$\sinh G \ge G$ which means
$$\begin{aligned}
d(z, K_c) &  > \frac{\sinh G(z)}{2e^{G(z)}|G'(z)|} \\
          &\ge \frac{G(z)}{2e^{G(z)}|G'(z)|} \\
          &=   \frac{G(z)}{2 \phi_c(z) |G'(z)|}
\end{aligned}$$ {#eq:}


# The Sierpiński Carpet: $z \to a z$

![The Sierpiński carpet.  Center $0$, zoom $1$.](sierpinski.png){width=50%}

The Sierpiński carpet is a figure consisting of $8$ copies of itself, each
shrunk by a factor of $\frac{1}{3}$ and arranged around the border of a
square (the central cell of the $3 \times 3$ grid is empty).  The iterated
function system for constructing the carpet can be turned into an escape
time process (see knighty's Kaleidoscopic Escape Time IFS[^knighty]),
whereby an input point $z$ is repeatedly transformed (including scaling
by $3$ at each step) until $z \to \infty$.

[^knighty]: <http://www.fractalforums.com/sierpinski-gasket/kaleidoscopic-(escape-time-ifs)/>

The transformations have finite translations, so the behaviour near $\infty$
is like $z \to 3 z$ (the translations are insignificant).  Fix a large $R$,
and count the iterations $n$ where $z_n$ is the first iterate such
$R \le |z_n|$.  Start iterating from $z_0$ at the pixel coordinates.

Now $R \le |z_n| < 3 R$, because if $3 R \le |z_n|$ then $R \le |z_{n-1}|$
by the behaviour near $\infty$, which contradicts the assumption that $z_n$
is the first iterate to exceed $R$.  The iteration count $n$ tells how
quickly $|z|$ has exceeded $R$, and the size of $|z|$ at this iteration can
be used to give a fractional iteration count: define $f$ by
$R \le |z_n| = 3^f R \le 3 R$, then $f = (\log|z_n| - \log R) / \log 3$
and the smooth iteration count is $m = n + 1 - f$.

Approaching the Sierpiński carpet fractal boundary, the smooth iteration
count increases without limit.  The rate of increase is linked to the
closeness to the boundary: an estimate of the distance is inversely
proportional to the spatial derivative of the smooth iteration count.
The spatial derivative of $m = n + 1 - f$ is equal to the spatial derivative
of $-f$ almost everywhere (the steps between dwell bands are a set of measure
zero), so
$$ d = \frac{|z_n| \log 3}{\frac{\partial z_n}{\partial c}}.$$ {#eq:}

For colouring images use $d / \epsilon$ where $\epsilon$ is the distance
between neighbouring pixels: when this value is less than $1$ the pixel
contains the fractal, when the value is greater than $1$ the pixel does not
contain the fractal.

The same arguments apply in the general case when $z \to a z$ near $\infty$,
with $a > 1$.  The smooth iteration count is
$$m = n + 1 - \frac{\log|z_n| - \log R}{\log a},$$ {#eq:}
the renormalized smooth iteration count is derived by differentiating
w.r.t. $R$, taking the limit as $R \to \infty$, then integrating again:
$$\mu = n - \frac{\log|z_n|}{\log a} + K$$ {#eq:}
where $K$ is an arbitrary constant of integration,
and the distance estimate is
$$d = \frac{|z_n| \log a}{\frac{\partial z_n}{\partial c}}.$$ {#eq:}


# The Mandelbrot Set: $z \to z^p$

![The Mandelbrot set.  Center $-\frac{3}{4}$, zoom $\frac{4}{5}$.](mandelbrot.png){width=50%}

The Mandelbrot set $M$ is defined by iterations over the complex numbers
$\mathbb{C}$ of $z \to z^2 + c$.  It can be shown that if
$|z| > \max\left\{|c|,2\right\}$ then $z \to \infty$, and a variant on the
argument shows all $|c| > 2$ are outside $M$. Thus $M$ is finite.

The addition is small for $c$ near $M$, so the behaviour near $\infty$ is
like $z \to z^2$ (the translation is insignificant).  Fix a large $R$,
and count the iterations $n$ where $z_n$ is the first iterate such that
$R \le |z_n|$.  Start the iterations from $z_0 = 0$.

Now $R \le |z_n| < R^2$, because if $R^2 \le |z_n|$ then $R \le |z_{n-1}|$
by the behaviour near $\infty$, which contradicts the assumption that $z_n$
is the first iterate to exceed $R$.  The iteration count $n$ tells how
quickly $|z|$ has exceeded $R$, and the size of $|z|$ at this iteration can
be used to give a fractional iteration count: define $f$ by
$R \le |z_n| = R^{2^f} < R^2$, then $f = \log(\log|z_n| / \log R) / \log 2$
and the smooth iteration count is $m = n + 1 - f$.

The renormalized smooth iteration count is derived by differentiating
w.r.t. $R$, taking the limit as $R \to \infty$, then integrating again:
$$\mu = n - \frac{\log \log|z_n|}{\log 2} + K$$ {#eq:}
where $K$ is an arbitrary constant of integration.

Approaching the Mandelbrot set fractal boundary, the smooth iteration
count increases without limit.  The rate of increase is linked to the
closeness to the boundary: an estimate of the distance is inversely
proportional to the spatial derivative of the smooth iteration count.
The spatial derivative of $\mu = n - g + K$ is equal to the spatial derivative
of $-g$ almost everywhere (the steps between dwell bands are a set of measure
zero), so the distance estimate is
$$d = \frac{|z_n| \log |z_n| \log 2}{\frac{\partial z_n}{\partial c}}.$$ {#eq:mde}

For colouring images use $d / \epsilon$ where $\epsilon$ is the distance
between neighbouring pixels: when this value is less than $\frac{1}{2}$ the pixel
contains the fractal, when the value is greater than $2$ the pixel does not
contain the fractal.  The factor of $4$ between these two figures is due to
the Koebe $\frac{1}{4}$ theorem, which bounds the distortion of conformal
maps, detailed discussion is beyond the scope of this document. See
Yumei Dang, Louis H. Kauffman and Dan Sandin,
"Hypercomplex Iterations: Distance Estimation and Higher Dimensional Fractals"[^hypercomplex].

[^hypercomplex]: <>

The same arguments apply in the general case when $z \to z^p$ near $\infty$,
with $p > 1$.  The smooth iteration count is
$$m = n + 1 - \frac{\log\frac{\log|z_n|}{\log R}}{\log p},$$ {#eq:}
the renormalized smooth iteration count is
$$\mu = n - \frac{\log \log|z_n|}{\log p} + K,$$ {#eq:}
and the distance estimate is
$$d = \frac{|z_n| \log |z_n| \log p}{\frac{\partial z_n}{\partial c}}.$$ {#eq:}


# Hybrid Fractals: $z \to a z^p$

![Mandelbrot set / Sierpiński carpet hybrid.  The Sierpiński carpet is implemented with $|\cdot|$ folding.  Center $0$, zoom $\frac{4}{5}$.](hybrid.png){width=50%}

One can make a hybrid of two fractals by interleaving their iteration
steps.  A hybrid of a fractal that escapes like $z \to a z$ with a fractal
that escapes like $z \to z^p$ will escape like $z \to a z^p$.

Fix a large $R$,
and count the iterations $n$ where $z_n$ is the first iterate such that
$R \le |z_n|$.  Start the iterations from $z_0 = 0$ and interleave the
exponential iterations first with the multiplicative iterations second.

Now $R \le |z_n| < aR^p$, because if $aR^p \le |z_n|$ then $R \le |z_{n-1}|$
by the behaviour near $\infty$, which contradicts the assumption that $z_n$
is the first iterate to exceed $R$.  The iteration count $n$ tells how
quickly $|z|$ has exceeded $R$, and the size of $|z|$ at this iteration can
be used to give a fractional iteration count.

Unfortunately defining $f$ by the simple $R \le |z_n| = a^f R^{p^f} < aR^p$
does not work as it isn't smooth.  One can see this by differentiating with
respect to $f$:
$$\frac{\partial}{\partial f} a^f R^{p^f} = a^f R^{p^f} \left(\log a + p^f \log p \log R\right)$$ {#eq:}
Setting $f = 1$ now gives a different value to setting $f = 0$ after making
the substitution $R \to a R^p$.

By Schröder 1870, a smooth interpolant is
$$R \le |z_n| = a^\frac{p^f-1}{p-1} R^{p^f} < a R^p$$ {#eq:}
Solving for $f$ gives
$$f = \log\left(\frac{\log a + (p - 1) \log |z_n|}{\log a + (p - 1) \log R}\right) / \log p$$ {#eq:}
and the smooth iteration count is $m = n + 1 - f$.
The renormalized smooth iteration count is found by differentiating w.r.t.
$R$, taking the limit as $R \to \infty$ before integrating again:
$$\mu = n - \frac{\log (\log a + (p - 1) \log |z_n|)}{\log p} + K$$ {#eq:}
where $K$ is an arbitrary constant of integration.  As before, the
distance estimate is inversely proportional to the spatial derivative:
$$d = \frac{|z_n| \log p \left(\log a + (p - 1) \log |z_n| \right)}{\frac{\partial z_n}{\partial c} (p-1)}$$ {#eq:}

If it is known that an iterate escapes like $z \to a z^p$ but the values of
the constants are not known, they can be estimated numerically by performing
two more iterations after escape:

$$p = \frac{\log |z_{n+2}| - \log |z_{n+1}|}{\log |z_{n+1}| - \log |z_n|}$$ {#eq:}
$$\log a = \log |z_{n+1}| - p \log |z_n|$$ {#eq:}


# Generalizations

The maths carries through without any difficulty to higher-dimensional
Menger sponge, Mandelbulb, etc.  Note that non-(complex)-analytic functions
need general Jacobian matrices of derivatives rather than a single (complex)
derivative as used for the Mandelbrot set.  A convenient implementation
technique is using vectors of extended dual numbers (with $2$ or more
orthogonal dual parts corresponding to the dimension of the space) for
automatic differentiation.

The form $z \to a z^p$, together with the earlier special forms for $p = 1$
and $a = 1$, are general enough to handle all polynomial iterations,
because near $\infty$ the highest power term dominates.  Hybrids of multiple
polynomial iterations are polynomial iterations themselves: for example if
$A : z \to z^p$ and $B : z \to a z$ then
$B \circ A : z \to a z^p$,
$A \circ B : z \to a^p z^p$,
$B \circ A \circ A : z \to a z^{2p}$,
$B \circ B \circ A : z \to a^2 z^p$,
and so on.  Any eventually-repeating hybrid can be constructed, provided that
the escape radius $R$ is large enough compared with the domain of evaluation
(the region of $c$ values) such that the non-repeating part is over by the
time the first $z_n$ escapes.


# Questions

## The $\log a$ Factor

The distance estimate derived here for $z \to a z$ differs from some of
the distance estimates in the implementations[^mandelbulber2] by a factor of $\log a$.

[^mandelbulber2]: <https://github.com/buddhi1980/mandelbulber2/blob/402b3cd8101732615b1e74943f990d8a85c59b03/mandelbulber2/src/compute_fractal.cpp#L431-L480>

## The $\log p$ Factor

The distance estimate derived here for $z \to z^p$ differ from some of
the distance estimates in the implementations and literature by a factor of $\log p$.

The distance estimate for the quadratic Mandelbrot set is apparently[^extensions]
$$d = \frac{1}{\frac{\partial \mu}{\partial c} \log 2}$$ {#eq:}
which works out to
$$d = \frac{|z_n| \log |z_n|}{\frac{\partial z_n}{\partial c}}$$
with the $\log 2$ cancelled compared to Equation @eq:mde.

[^extensions]: <https://fractalforums.org/fractal-mathematics-and-new-theories/28/extension-of-numerical-de-bounds-to-other-powersdimensions/3004>

## Upper and Lower Bounds

For a sphere marching ray tracer, one needs a lower bound on the distance.
If the distance is ever overestimated, overstepping badness results.  The
distance estimates derived here are only approximate, not strict bounds.

The Hypercomplex book chapter 3.6 has a lower bound for the
distance of a point $z$ to a quadratic Julia set $K_c$ of:
$$d(z, K_c) > \frac{\sinh{G(z)}}{2\exp{G(z)} |G'(z)|} \approx \frac{1}{2|z_n|^\frac{1}{2^n}} \frac{|z_n| \log |z_n|}{|z_n'|}$$ {#eq:}
whose extra factor of $|z_n|^\frac{1}{2^n} \to 1+$ as $n \to \infty$, for
$\mu > 7$ the error from ignoring this term is less than 1%, but it may need
to be taken into account further from the fractal.

The Hypercomplex book chapter 7.5 has a lower bound for the distance to
a hypercomplex Julia set $K_q$ of:
$$d(z, K_q) > c \frac{|z_n| \log |z_n|}{|z_n'|}$$
where $c$ is a positive real constant.  

# References

## Renormalizing the Mandelbrot Escape

> The scale-dependence on the escape radius can be (partly) removed by
> differentiating the above formula with respect to the escape radius,
> taking the limit to infinity, and then integrating the first term of
> the resulting differential equation. The result is a renormalized,
> scale-independent, quasi-integral-valued iteration count.
> -- Linas Vepstas (1997) "Renormalizing the Mandelbrot Escape"[^renorm]


## M-Set Derivatives

> The "distance estimator" is the inverse infinitesimal flow of the
> iteration number. It gets this name because it provides a rough
> estimate of how far away an exterior point is from the boundary of the
> M-Set. The smooth (real-valued) iteration count is given by
> $\mu = n+1-\log(\log(|z|))/\log 2$, as demonstrated in the Escape
> Theory Room (which is, in turn, the logarithm of the Douady-Hubbard
> potential). Taking the derivative of $\mu$ w.r.t. $c$ we get
> $d\mu/dc = d|z|/dc / (|z| \log |z| \log 2)$. The "distance estimator"
> is one over this quantity. 
> -- Linas Vepstas (1997, 2000) "M-Set Derivatives"[^mderiv]

[^mderiv]: <https://linas.org/art-gallery/mderive/mderive.html>

## Ueber iterirte Functionen

> Setzt man z.B.: $$\phi(z) = a z^n,$$ {#eq:}
> so findet man leicht direct:
> $$\phi^r(z) = a^\frac{n^r-1}{n-1} z^{n^r}.$$ {#eq:}
> -- Ernst Schröder (1870). "Ueber iterirte Functionen".
> Math. Ann. 3 (2): 296–322. doi:10.1007/BF01443992.[^iterirte]

[^iterirte]: <https://doi.org/10.1007%2FBF01443992>
